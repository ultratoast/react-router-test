var mongoose = require('mongoose')

var postSchema = new mongoose.Schema({
	name: String,
	postDate: {type: Date, default: Date.now},
	category: String,
	content: String,
	isPublic: Boolean,
	upvotes: {type: Number, default: 0}
})

mongoose.model('Post', postSchema)