preferred_syntax = :sass
http_path = 'dist/'
css_dir = 'dist'
sass_dir = 'static/scss'
images_dir = 'static/img'
javascripts_dir = 'static/js'
relative_assets = true
output_style = :compressed