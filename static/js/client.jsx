var ReactDOM = require('react-dom'),
	Router = require('react-router/lib/Router'),
	browserHistory = require('react-router/lib/browserHistory'),
	routes = require('components/routes.jsx'),
	content = document.getElementById('content')

ReactDOM.render(<Router routes={routes} history={browserHistory}/>,content)