module.exports = {
  context: __dirname,
    entry: ["./static/js/client.jsx"],
    output: {
        path: __dirname + "/dist",
        filename: "bundle.js"
    },
 module: {
   loaders: [
     {
      test: /\.jsx?$/,
      exclude: /(node_modules|bower_components|compiled)/,
      loader: 'babel', // 'babel-loader' is also a legal name to reference
      query: {
        presets: ['react', 'es2015']
      }
    },
     {
      test: /\.json$/,
      loaders: ['json']
     }
   ]
 },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json']
  }
};