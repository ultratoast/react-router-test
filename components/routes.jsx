const React = require('react'),
	Route = require('react-router/lib/Route'),
	DefaultLayout = require('components/layouts/defaultlayout.jsx'),
	HomeIndex = require('components/home/index.jsx'),
	BlogIndex = require('components/blog/index.jsx'),
	AddBlog = require('components/blog/add.jsx'),
	ShowBlog = require('components/blog/show.jsx'),
	EditBlog = require('components/blog/edit.jsx'),
	BlogArchive = require('components/blog/archive.jsx')

export default (
	<Route path="/" component={DefaultLayout}>
		<Route component={HomeIndex}/>
		<Route path="blog" component={BlogIndex}>
			<Route path="add" component={AddBlog}/>
			<Route path=":id" component={ShowBlog}/>
			<Route path=":id/edit" component={EditBlog}/>
			<Route path="archive" component={BlogArchive}/>
		</Route>
	</Route>
)