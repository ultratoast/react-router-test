var React = require('react'),
	SiteNav = require('components/layouts/nav.jsx')

var DefaultLayout = React.createClass({
	render: function() {
		return (
			<html>
				<head>
					<title>{this.props.title}</title>
					<link rel="stylesheet" href="/dist/styles.css"/>
				</head>
				<body>
					<SiteNav/>
					<section id="content">{this.props.children}</section>
					<script src="/dist/bundle.js"></script>
				</body>
			</html>
		)
	}
})

module.exports = DefaultLayout
