var React = require('react'),
	Upvote = require('components/blog/upvote.jsx'),
	Link = require('react-router/lib/Link')

var Post = React.createClass({
	getInitialState: function () {
	  return {blog: this.props.blog,postDate: this.props.postDate}
	},
	componentDidMount: function() {
		console.log('mounted!')
	},
	render: function(){
		var blog = this.state.blog,
			postDate = this.state.postDate ? <strong>blog.postDate.toTimeString()</strong> : false
		return (
			<li className="blog">	
				<Link to={"/blogs/"+blog._id}><h1>{blog.name}</h1></Link>
				{postDate}
				<br />
				<span className="upvotes">Upvotes: {blog.upvotes}</span>
				<p>{blog.content}</p>	
				<Upvote blog={blog}/>
			</li>
		)
	}
})

module.exports = Post