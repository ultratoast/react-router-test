var React = require('react'),
	Post = require('components/blog/post.jsx')

var BlogHome = React.createClass({
	getInitialState: function () {
	  return {blogs: this.props.blogs}
	},
	render: function(){
		let blogs = this.state.blogs
		if (blogs && blogs.length > 0) {
			blogs = blogs.map(function(blog,i){
				let isPublic = blog.isPublic ? <Post key={i} blog={blog}/> : null
				return (
					isPublic
				)
			}.bind(this))
			blogs = blogs.filter(function(blog){
				if (blog) {
					return true
				} else {
					return false
				}
			})
			if (blogs.length > 0) {
				return (
					<div>
						<h2>Blog Home</h2>
						<ul className="blogs">
							{blogs}
						</ul>
					</div>
				)
			} else {
				return (
					<div>
						<h2>Blog Home</h2>
						<p>Sorry, No public blogs found!</p>
					</div>
				)
			}
		} else {
			return (
				<p>Sorry, No data!</p>
			)
		}
	}
})

module.exports = BlogHome