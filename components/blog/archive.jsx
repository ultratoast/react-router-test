var React = require('react'),
	Post = require('components/blog/post.jsx')

var BlogArchive = React.createClass({
	getInitialState: function () {
	  return {blogs: this.props.blogs}
	},
	render: function() {
		let blogs = this.state.blogs
		if (blogs && blogs.length > 0) {
			blogs = blogs.map(function(blog,i){
				return (
					<Post key={i} blog={blog} />
				)
			}.bind(this))
			return (
				<div>
					<h2>Blog Archive</h2>
					<ul className="blogs">
						{blogs}
					</ul>
				</div>
			)
		} else {
			return (
				<div>
					<p>No Blogs Found!</p>
				</div>
			)
		} 
	}
})

module.exports = BlogArchive