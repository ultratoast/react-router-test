const Express = require('express'),
	apirouter = Express.Router(),
	BlogAPI = require('components/api/blog.jsx')

//base route
apirouter.get('/',(req,res) => {
	return 
})

//blog-specific api routes
apirouter.get('/blog',(req,res) => {
	return BlogAPI.getBlogHome(req,res)
})

apirouter.get('/blog/archive',(req,res) => {
	return BlogAPI.getBlogArchive(req,res)
})

apirouter.get('/blog/:id',(req,res) => {
	return BlogAPI.getPostById(req,res)
})

apirouter.post('/blog/add',(req,res) => {
	return BlogAPI.addPost(req,res)
})

apirouter.post('/blog/delete',(req,res) => {
	return BlogAPI.deletePost(req,res)
})

apirouter.post('/blog/:id/edit',(req,res) => {
	return BlogAPI.editPost(req,res)
})

apirouter.post('/blog/:id/upvote',(req,res) => {
	return BlogAPI.editPost(req,res)
})

module.exports = apirouter