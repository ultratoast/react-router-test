const mongoose = require('mongoose')

const BlogAPI = {
	getBlogHome : function(req,res) {
		mongoose.model('Post').find({},function(err,blog){
			if (err) {
				return console.error(err)
			} else{
				//respond with both html & json, json needs 'Accept:application/json;' in request header
				res.format({
					json: function(){
						res.json(blog)
					}
				})
			}
		})	
	},

	getBlogArchive : function(req,res) {
		mongoose.model('Post').find({},function(err,blog){
			if (err) {
				return console.error(err)
			} else {
				res.format({
					json: function() {
						res.json(blog)
					}
				})
			}
		})	
	},

	addPost : function(req,res) {
		let name = req.body.name,
			isPublic = req.body.isPublic,
			content = req.body.content
		mongoose.model('Post').create({
			name: name,
			isPublic: isPublic,
			content: content
		},function(err,post){
			if (err) {
				res.send('There has been an error uploading your post, please try again')
			} else {
				console.log('POST creating new post: '+post)
				res.format({
					json: function(){
						res.json(post)
					}
				})
			}
		})	
	},

	getPostById : function(req,res) {
		mongoose.model('Post').findById(req.id, function(err,post){
			if (err) {
				console.log('GET error: '+err)
			} else {
				console.log('GET retrieving '+post._id)
				let postDate = post.postDate.toISOString()
				postDate = postDate.substring(0,postDate.indexOf('T'))
				res.format({
					json: function() {
						res.json(post)
					}
				})
			}
		})
	},

	addPostUpvote : function(req,res) {
		mongoose.model('Post').findById(req.body.id, function(err, post){
			post.update({
				upvotes: post.upvotes + 1
			}, function(err, post){
				if (err) {
					res.send('There was a problem with the database')
				} else {
					res.format({
						json: function() {
							res.json(post)
						}
					})
				}
			})
		})
	},

	editPost : function(req,res) {
		let name = req.body.name,
			isPublic = req.body.isPublic,
			content = req.body.content

		mongoose.model('Post').findById(req.body.id, function(err,post){
			post.update({
				name: name,
				isPublic: isPublic,
				content: content
			}, function(err, post){
				if (err) {
					res.send('There was a problem updating the database')
				} else {
					res.format({
						json: function(){
							res.json(post)
						}
					})
				}
			})
		})
	},

	deletePost : function(req, res) {
		mongoose.model('Blog').findById(req.id, function(err,blog){
			if (err) {
				return console.error(err)
			} else {
				blog.remove(function(err, blog){
					if (err) {
						return console.error(err)
					} else {
						res.format({
							html: function(){
								res.redirect('/blogs/home')
							},
							json: function(){
								res.json({
									message: 'Deleted', 
									item: blog
								})
							}
						})
					}
				})
			}
		})
	}
}

module.exports = BlogAPI