"use strict"

const Express = require('express'),
	React = require('react'),
	ReactDOMServer = require('react-dom/dist/react-dom-server'),
	Router = require('react-router'),
	RouterContext = require('react-router/lib/RouterContext'),
	fetch = require('node-fetch'),
	routes = require('components/routes.jsx'),
	app = Express(),
	db = require('models/db.js'),
	post = require('models/post.js'),
	apirouter = require('components/api/router.jsx')

app.set('view engine', 'html')

app.use('/api',apirouter)

app.get('*', (req,res) => {
	let apidata = fetch('/api'+req.url)
		.then(function(data){
			return data
		})
	res.send(ReactDOMServer.renderToString(<RouterContext routes={routes} props={apidata} location={req.url}/>))
	// Router.match({routes, location: req.url}, (err,redirect,props) => {
	// 	let code = 200
	// 	if (err) {
	// 		code = 500
	// 		console.log(code+' '+err)
	// 		res.status(code).send(err)
	// 	} else if (redirect) {
	// 		code = 302
	// 		console.log(code+' '+redirect.pathname)
	// 		res.redirect(code, redirect.pathname + redirectLocation.search)
	// 	} else if (apidata) {
	// 		console.log(code+' '+req.url)
	// 		res.status(code).send(ReactDOMServer.renderToString(<Router routes={routes} props={apidata}/>))
	// 	} else {
	// 		code = 404
	// 		console.log(code+' '+req.url+' not found')
	// 		res.status(code).send('Not Found')
	// 	}
	// })
})

app.listen(3000, function(){
	console.log('listening on port 3000')
})
